package com.example.mylawyer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import com.example.mylawyer.databinding.ActivityEditUserBinding
import com.example.mylawyer.databinding.ActivityLawyerDetailBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EditUserActivity : AppCompatActivity() {

    private lateinit var name: EditText
    private lateinit var phone: EditText
    private lateinit var binding: ActivityEditUserBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        val clientData = intent.getSerializableExtra("client") as Client

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_user)

        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)

        //clientData.idAuth?.let { Log.d("JAJAJA", it) }


        binding.submit.setOnClickListener{
            val idAuth = clientData.idAuth

            val client = Client(clientData.id,idAuth,name.toString(),phone.toString(),clientData.emailAddress,clientData.hasMeetings)

            if (idAuth != null) {
                updateClient(idAuth,client)
            }

            val eBuilder = AlertDialog.Builder(this)
            eBuilder.setTitle("Update Your Info")
            eBuilder.setIcon(R.drawable.ic_baseline_check_24)
            eBuilder.setMessage("You have successfully updated your info ")
            eBuilder.setPositiveButton("Ok")
            {
                    Dialog, which->
                finish()
            }
            val createBuild = eBuilder.create()
            createBuild.show()
        }

    }

    private fun updateClient(idAuth: String, client: Client) {
        val retroInstance = RetroInstance.getRetroInstance().create(RetroService::class.java)
        val call = retroInstance.updateClient(idAuth, client)
        call.enqueue(object : Callback<ClientResponse?> {
            override fun onFailure(call: Call<ClientResponse?>, t: Throwable) {
            }
            override fun onResponse(call: Call<ClientResponse?>, response: Response<ClientResponse?>) {
                if(response.isSuccessful) {


                } else {
                }
            }


        })
    }
}