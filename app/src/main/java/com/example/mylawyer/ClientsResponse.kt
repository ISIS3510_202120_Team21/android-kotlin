package com.example.mylawyer

import com.google.gson.annotations.SerializedName

data class ClientsResponse(
    @SerializedName("status")
    var status: String? = null,
    @SerializedName("results")
    var results: Int = 0,
    @SerializedName("data")
    var data: Dataa? = null
)

data class Dataa(
    @SerializedName("data")
    var dataList: ArrayList<Client>
)

data class Client(
    @SerializedName("_id")
    var id: String? = null,
    @SerializedName("idAuth")
    var idAuth: String? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("phoneNumber")
    var phoneNumber: String? = null,
    @SerializedName("emailAddress")
    var emailAddress: String? = null,
    @SerializedName("hasMeetings")
    var hasMeetings: Boolean = false,
)

data class ClientResponse(val code: Int?, val meta: String?, val data: Client?)