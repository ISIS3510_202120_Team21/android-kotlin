package com.example.mylawyer

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mylawyer.databinding.ActivityAppointmentsListBinding
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AppointmentsList : AppCompatActivity() {

    private lateinit var  binding: ActivityAppointmentsListBinding
    private lateinit var adapter:AppointmentAdapter
    private val appointments = mutableListOf<Appointment>()

    var context: Context? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding = ActivityAppointmentsListBinding.inflate(layoutInflater)
        initRecycleView()

    }

    private fun initRecycleView() {
        adapter = AppointmentAdapter(appointments)
        binding.rvAppointments.layoutManager = LinearLayoutManager(this)
        binding.rvAppointments.adapter = adapter
    }

    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }

    private fun getRetrofit():Retrofit{
        return Retrofit.Builder()
            .baseUrl("https://mylawyer-flutter.herokuapp.com/api/appointments/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun getClientAppointments(){
        val online = context?.let { isOnline(it) }
        if(online==true){
            val prefs = getSharedPreferences("PREFERENCES", Context.MODE_PRIVATE)
            val gson = Gson()
            val json = prefs.getString("User","")
            val client = gson.fromJson(json, Client::class.java)
            val clientId = client.idAuth
            CoroutineScope(Dispatchers.IO).launch {
                val call = getRetrofit().create(AppointmentsAPIService::class.java).getAllAppointments("?clientId=$clientId")
                val appointmentsResponse: GetAppointmentsResponse? = call.body()
                runOnUiThread {
                    if(call.isSuccessful){
                        val appointmentsList = appointmentsResponse?.data?.dataList ?: emptyList()
                        appointments.clear()
                        appointments.addAll(appointmentsList)
                        adapter.notifyDataSetChanged()
                    }else{
                        showError()
                    }
                }
            }
        }else{
            showError()
        }

    }

    private fun showError() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage("Your internet connection is not stable. Please check your internet connection or try again later.")
        builder.setPositiveButton("Accept", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
}