    package com.example.mylawyer

    import android.content.Context
    import android.net.ConnectivityManager
    import android.net.NetworkCapabilities
    import androidx.appcompat.app.AppCompatActivity
    import android.os.Bundle
    import android.util.Log
    import android.widget.SeekBar
    import android.widget.Toast
    import androidx.appcompat.app.AlertDialog
    import androidx.core.util.lruCache
    import androidx.recyclerview.widget.LinearLayoutManager
    import com.example.mylawyer.databinding.ActivityListaAbogadosFiltroBinding
    import kotlinx.coroutines.CoroutineScope
    import kotlinx.coroutines.Dispatchers
    import kotlinx.coroutines.launch
    import retrofit2.Response
    import retrofit2.Retrofit
    import retrofit2.converter.gson.GsonConverterFactory

    class ListaAbogadosFiltro : AppCompatActivity(), SeekBar.OnSeekBarChangeListener {

        var context: Context? = null
        private lateinit var binding: ActivityListaAbogadosFiltroBinding
        private lateinit var adapter: LawyerAdapter
        private var lawyers = mutableListOf<Lawyer>()
        private var lawyersBeggining = mutableListOf<Lawyer>()
        private var lawyersOnCache = lruCache<String,Lawyer>((Runtime.getRuntime().maxMemory()/1024).toInt()/8)
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            context = applicationContext
            binding = ActivityListaAbogadosFiltroBinding.inflate(layoutInflater)
            setContentView(binding.root)
            val actionBar = supportActionBar
            actionBar!!.title = "MyLawyer"
            actionBar.setDisplayHomeAsUpEnabled(true)
            binding.seekBarRating.setOnSeekBarChangeListener(this)
            initRecycleView()
            getLawyers()
        }

        private fun initRecycleView() {
            adapter = LawyerAdapter(lawyers)
            binding.rvLawyers.layoutManager = LinearLayoutManager(this)
            binding.rvLawyers.adapter = adapter
        }

        private fun getRetrofit():Retrofit{
            return Retrofit.Builder()
                .baseUrl("https://mylawyer-flutter.herokuapp.com/api/lawyers/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

        fun isOnline(context: Context): Boolean {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (connectivityManager != null) {
                val capabilities =
                    connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                        return true
                    }
                }
            }
            return false
        }


        private fun getLawyers(){
            val online = context?.let { isOnline(it) }
            if(online==true){
                CoroutineScope(Dispatchers.IO).launch {
                    val call: Response<LawyersResponse> = getRetrofit().create(LawyersAPIService::class.java).getAllLawyers("")
                    val lawyersResponse: LawyersResponse? = call.body()
                    runOnUiThread{
                        if(call.isSuccessful){
                            val lawyersList = lawyersResponse?.data?.dataList ?: emptyList()
                            lawyers.clear()
                            lawyers.addAll(lawyersList)
                            lawyersBeggining.addAll(lawyersList)
                            adapter.notifyDataSetChanged()
                            lawyersOnCache.evictAll()
                            for (lawyer in lawyersList){
                                lawyersOnCache.put(lawyer.id,lawyer)
                            }
                            //recyclerview
                        }else{
                            showError()
                        }
                    }

                }
            }
            else{
                lawyers.clear()
                for (lawyer in lawyersOnCache.snapshot().values){
                    lawyers.add(lawyer)
                }
                if(lawyers.isNotEmpty()){
                    lawyersBeggining.addAll(lawyers)
                    adapter.notifyDataSetChanged()
                }else{
                    showError()
                }
            }
        }

        private fun showError() {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Error")
            builder.setMessage("Your internet connection is not stable. Please check your internet connection or try again later.")
            builder.setPositiveButton("Accept", null)
            val dialog: AlertDialog = builder.create()
            dialog.show()
        }

        override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
            val lawyersList = mutableListOf<Lawyer>()
            for (lawyer in lawyersBeggining) {
                if(lawyer.rating >= p1){
                    lawyersList.add(lawyer)
                }
            }
            Toast.makeText(getApplicationContext(), p1.toString(),Toast.LENGTH_LONG).show()
            lawyers.clear()
            lawyers.addAll(lawyersList)
            adapter.notifyDataSetChanged()
        }

        override fun onStartTrackingTouch(p0: SeekBar?) {
            return
        }

        override fun onStopTrackingTouch(p0: SeekBar?) {
            return
        }
    }