package com.example.mylawyer

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.mylawyer.databinding.ItemLawyerBinding

class LawyerViewHolder(view:View):RecyclerView.ViewHolder(view) {

    private val binding = ItemLawyerBinding.bind(view)

    fun bind(lawyer:Lawyer){
        binding.nameString.setText("Name: ".plus(lawyer.name))
        binding.fieldString.setText("Field: ".plus(lawyer.field))
        binding.hourlyFeeString.setText("Hourly Fee: ".plus(lawyer.hourlyFee.toString()))
        binding.phoneNumberString.setText("Phone number: ".plus(lawyer.phoneNumber.toString()))
        binding.ratingString.setText("Rating: ".plus(lawyer.rating.toString()))
        binding.imageViewAvatar.setImageResource(R.drawable.ic_person_244)
    }
}