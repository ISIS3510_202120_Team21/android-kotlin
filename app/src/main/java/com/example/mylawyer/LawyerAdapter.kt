package com.example.mylawyer

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class LawyerAdapter(private val lawyers:List<Lawyer>):RecyclerView.Adapter<LawyerViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LawyerViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return LawyerViewHolder(layoutInflater.inflate(R.layout.item_lawyer, parent, false))
    }

    override fun onBindViewHolder(holder: LawyerViewHolder, position: Int) {
        val item: Lawyer = lawyers[position]
        holder.bind(item)
        holder.itemView.setOnClickListener{ v->
            val intent = Intent(v.context, LawyerDetail::class.java)
            intent.putExtra("lawyer", item)
            v.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int = lawyers.size
}