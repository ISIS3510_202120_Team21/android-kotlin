package com.example.mylawyer


import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface ClientsAPIService {
    @GET
    suspend fun getAllClients(@Url url: String):Response<ClientsResponse>
}