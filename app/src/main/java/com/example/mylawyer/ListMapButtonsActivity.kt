package com.example.mylawyer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class ListMapButtonsActivity : AppCompatActivity() {
    private lateinit var mapabtn: Button
    private lateinit var listabtn: Button
    private lateinit var reminderbtn: Button
    private lateinit var profilebtn: Button
    private lateinit var appointmentsbtn: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_map_buttons)
        mapabtn = findViewById(R.id.map)
        listabtn = findViewById(R.id.list)
        //reminderbtn = findViewById(R.id.reminder)
        profilebtn = findViewById(R.id.profile)
        appointmentsbtn = findViewById(R.id.appointmentsBtn)
        //val intentValue = intent.getStringExtra("Data")
        setup()
    }

    private fun setup()
    {
        title = "MyLawyer"

        mapabtn.setOnClickListener{
            val intent = Intent(this,LawyersMap::class.java)
            startActivity(intent)
        }
        listabtn.setOnClickListener{
            val intent = Intent(this,ListaAbogadosFiltro::class.java)
            startActivity(intent)
        }

        //reminderbtn.setOnClickListener{
          //  val intent = Intent(this,ReminderAppointment::class.java).apply {
            //    putExtra("Data",a)
            //}
            //startActivity(intent)
        //}

        profilebtn.setOnClickListener{
            val intent = Intent(this,ProfileActivity::class.java)
            startActivity(intent)
        }

        appointmentsbtn.setOnClickListener{
            val intent = Intent(this,AppointmentsList::class.java)
            startActivity(intent)
        }
    }
}