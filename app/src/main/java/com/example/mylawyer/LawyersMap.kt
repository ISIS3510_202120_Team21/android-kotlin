package com.example.mylawyer

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class LawyersMap : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener {

    private lateinit var map: GoogleMap

    private var lawyers = mutableListOf<Lawyer>()

    companion object{
        const val REQUEST_CODE_LOCATION = 0
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lawyers_map)
        val actionBar = supportActionBar
        actionBar!!.title = "Lawyers Map"
        actionBar.setDisplayHomeAsUpEnabled(true)
        createFragment()
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://mylawyer-flutter.herokuapp.com/api/lawyers/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun getLawyers(){
        CoroutineScope(Dispatchers.IO).launch {
            val call: Response<LawyersResponse> = getRetrofit().create(LawyersAPIService::class.java).getAllLawyers("")
            val lawyersResponse: LawyersResponse? = call.body()
            runOnUiThread{
                if(call.isSuccessful){
                    val lawyersList = lawyersResponse?.data?.dataList ?: emptyList()
                    lawyers.clear()
                    lawyers.addAll(lawyersList)
                    //Create Marker
                    val iterator = lawyers.listIterator()
                    for(item in iterator)
                    {
                        val a = item.latitude
                        val b = item.longitude
                        if (a != null && b!=null) {
                            val coordinates = LatLng(a.toDouble(),b.toDouble())
                            val marker : MarkerOptions = MarkerOptions().position(coordinates).title(item.name)
                            map.addMarker(marker)
                        }
                    }

                    //map.animateCamera(
                        //  CameraUpdateFactory.newLatLngZoom(coordinates,18f),
                        //4000,
                        //null
                    //)

                }else{
                    showError()
                }
            }

        }
    }

    private fun showError() {
        Toast.makeText(this, "An error has occurred, please try again later", Toast.LENGTH_SHORT).show()
    }

    private fun createFragment()
    {
        val mapFragment : SupportMapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        getLawyers()
        map.setOnMyLocationButtonClickListener(this)
        enableLocation()
    }

    private fun isLocationPermissionGranted()=ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED


    private fun enableLocation()
    {
        if(!::map.isInitialized) return
        if(isLocationPermissionGranted())
        {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            map.isMyLocationEnabled = true
        }
        else
        {
            requestLocationPermission()
        }
    }

    private fun requestLocationPermission()
    {

        if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_FINE_LOCATION))
        {
            Toast.makeText(this,"Accept the permissions to access your location", Toast.LENGTH_SHORT).show()
        }
        else{
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_CODE_LOCATION)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            REQUEST_CODE_LOCATION-> if(grantResults.isNotEmpty() && grantResults[0]==PackageManager.PERMISSION_GRANTED)
            {
                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return
                }
                map.isMyLocationEnabled = true
            }else{
                Toast.makeText(this,"Accept the permissions to access your location", Toast.LENGTH_SHORT).show()

            }
            else ->{}

        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        if(!::map.isInitialized) return
        if(!isLocationPermissionGranted()){
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            map.isMyLocationEnabled = false
            Toast.makeText(this,"Accept the permissions to access your location", Toast.LENGTH_SHORT).show()

        }
    }

    override fun onMyLocationButtonClick(): Boolean {
        Toast.makeText(this,"You are here!!", Toast.LENGTH_SHORT).show()
        return false
    }
}