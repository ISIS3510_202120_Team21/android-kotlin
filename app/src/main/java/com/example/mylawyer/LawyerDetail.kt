package com.example.mylawyer

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.mylawyer.databinding.ActivityLawyerDetailBinding
import com.example.mylawyer.databinding.ActivityListaAbogadosFiltroBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.StringBuilder
import java.util.*
import android.content.SharedPreferences
import com.google.gson.Gson
import kotlin.collections.ArrayList


class LawyerDetail : AppCompatActivity() {

    private lateinit var binding: ActivityLawyerDetailBinding
    private lateinit var lawyer: Lawyer
    var selectedDay = 0
    var selectedMonth = 0
    var selectedYear = 0
    var selectedHour = 0
    var selectedMinute = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lawyer_detail)
        val lawyerData = intent.getSerializableExtra("lawyer") as Lawyer
        lawyerData.name?.let { Log.d("LAWYER", it) }
        binding = ActivityLawyerDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        lawyer = lawyerData
        binding.textViewName.setText(lawyerData.name)
        binding.textViewSpecialization.setText(lawyerData.field)
        binding.textViewEmail.setText(lawyerData.emailAddress)
        binding.textViewPhone.setText(lawyerData.phoneNumber.toString())
        var stringBuilder = StringBuilder()
        binding.textViewExperience.setText(stringBuilder.append(lawyerData.yearOfExperience.toString()).append("\n Years of experience"))
        stringBuilder = StringBuilder()
        binding.textViewWonCases.setText(stringBuilder.append(lawyerData.casesWon.toString()).append("\n Cases won"))
        stringBuilder = StringBuilder()
        binding.textViewRating.setText(stringBuilder.append(lawyerData.rating.toString()).append("\n Rating"))
        binding.buttonAppointment.setOnClickListener{
            showDatePickerDialog()
        }

        binding.buttonHire.setOnClickListener{
            val idAuth = lawyerData.idAuth
            val lawyer = Lawyer(lawyerData.id,lawyerData.idAuth,
                lawyerData.name,lawyerData.phoneNumber,lawyerData.emailAddress,lawyerData.casesWon,
                lawyerData.field,lawyerData.hourlyFee,lawyerData.rating,lawyerData.numberOfRatings,
                lawyerData.latitude,lawyerData.longitude,lawyerData.views+1,lawyerData.yearOfExperience)
            if (idAuth != null) {
                updateUser(idAuth,lawyer)
            }
            val eBuilder = AlertDialog.Builder(this)
            eBuilder.setTitle("Hire a Lawyer")
            eBuilder.setIcon(R.drawable.ic_baseline_check_24)
            eBuilder.setMessage("You have successfully hired: "+lawyerData.name+ ".")
            eBuilder.setPositiveButton("Ok")
            {
                Dialog, which->
                finish()
            }
            val createBuild = eBuilder.create()
            createBuild.show()
        }

    }

    private fun showDatePickerDialog() {
        val datePicker = DatePickerFragment { day, month, year -> onDateSelected(day, month, year) }
        datePicker.show(supportFragmentManager, "datePicker")
    }

    fun onDateSelected(day:Int, month:Int, year:Int){
        selectedDay = day
        selectedMonth = month
        selectedYear = year
        showTimePickerDialog()
    }

    private fun showTimePickerDialog() {
        val timePicker = TimePickerFragment {time -> onTimeSelected(time)}
        timePicker.show(supportFragmentManager, "timePicker")

    }
    fun addAppointment(appointmentData: AppointmentsResponse, onResult: (AppointmentsResponse?) -> Unit){
        val retrofit = ServiceBuilder.buildService(AppointmentsAPIService::class.java)
        appointmentData.data?.dataList?.let {
            retrofit.addAppointment(it).enqueue(
                object : Callback<AppointmentsResponse> {
                    override fun onFailure(call: Call<AppointmentsResponse>, t: Throwable) {
                        onResult(null)
                    }

                    override fun onResponse( call: Call<AppointmentsResponse>, response: Response<AppointmentsResponse>) {
                        val addedAppointment = response.body()
                        onResult(addedAppointment)
                    }
                }
            )
        }
    }

    private fun onTimeSelected(time: String){
        val dateHour = "$selectedYear-$selectedMonth-$selectedDay"+"T$time:00.000z"
        val prefs = getSharedPreferences("PREFERENCES", Context.MODE_PRIVATE)
        val gson = Gson()
        val json = prefs.getString("User","")
        val client = gson.fromJson(json, Client::class.java)
        val clientId = client.idAuth
        val name = lawyer.name
        val specialty = lawyer.field
        val lawyerId = lawyer.idAuth
        val appointment = Appointment(dateHour=dateHour,description = "Appointment with lawyer $name"+". $specialty", clientId = clientId, lawyerId = lawyerId)

        val appointmentResponse = AppointmentsResponse("true",
            DataAppointments(appointment)
        )
        Log.d("body",appointmentResponse.toString())
        addAppointment(appointmentResponse){
            if (it?.data?.dataList?.clientId != null) {
                // it = newly added user parsed as response
                // it?.id = newly added user ID
            } else {
                showAlert()
            }
        }
    }

    private fun showAlert(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage("An error has ocurred while creating the appointment.")
        builder.setPositiveButton("Accept", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun showSuccessAlert(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Success!")
        builder.setMessage("Appointment succesfully created!")
        builder.setPositiveButton("Accept", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }



    private fun updateUser(idAuth: String, lawyer: Lawyer) {
        val retroInstance = RetroInstance.getRetroInstance().create(RetroService::class.java)
        val call = retroInstance.updateLawyer(idAuth, lawyer)
        call.enqueue(object : Callback<LawyerResponse?> {
            override fun onFailure(call: Call<LawyerResponse?>, t: Throwable) {
            }
            override fun onResponse(call: Call<LawyerResponse?>, response: Response<LawyerResponse?>) {
                if(response.isSuccessful) {


                } else {
                }
            }
        })
    }
}