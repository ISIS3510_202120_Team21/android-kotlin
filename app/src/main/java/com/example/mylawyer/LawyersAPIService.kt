package com.example.mylawyer


import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface LawyersAPIService {
    @GET
    suspend fun getAllLawyers(@Url url: String):Response<LawyersResponse>
}