package com.example.mylawyer

import retrofit2.Call
import retrofit2.http.*

interface RetroService {


    @PATCH("lawyers/{idAuth}")
    @Headers("Content-Type:application/json")
    fun updateLawyer(@Path("idAuth") idAuth:String, @Body params: Lawyer): Call<LawyerResponse>


    @PATCH("clients/{idAuth}")
    @Headers("Content-Type:application/json")
    fun updateClient(@Path("idAuth") idAuth:String, @Body params: Client): Call<ClientResponse>


}