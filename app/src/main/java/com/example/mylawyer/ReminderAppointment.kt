package com.example.mylawyer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ReminderAppointment : AppCompatActivity() {

    private var clients = mutableListOf<Client>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reminder_appointment)
        val actionBar = supportActionBar
        actionBar!!.title = "Reminders"
        actionBar.setDisplayHomeAsUpEnabled(true)

        val intentValue = intent.getStringExtra("Data")
        getClients(intentValue.toString())
    }


    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://mylawyer-flutter.herokuapp.com/api/clients/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun getClients(aa: String){
        CoroutineScope(Dispatchers.IO).launch {
            val call: Response<ClientsResponse> = getRetrofit().create(ClientsAPIService::class.java).getAllClients("")
            val clientsResponse: ClientsResponse? = call.body()
            runOnUiThread{
                if(call.isSuccessful){
                    val clientsList = clientsResponse?.data?.dataList ?: emptyList()
                    clients.clear()
                    clients.addAll(clientsList)
                    //Create Marker
                    val iterator = clients.listIterator()

                    for(item in iterator)
                    {
                        val a = item.emailAddress
                        if(a==aa && item.hasMeetings)
                        {
                            showSuccessAlert()
                        }
                        else if (a==aa){
                            showSuccessAlert2()
                        }

                    }
                }else{
                    showError()
                }
            }

        }
    }

    private fun showSuccessAlert(){

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Appointment Coming")
        builder.setMessage("Do not forget to attend your meeting")
        val dialog: AlertDialog = builder.create()
        dialog.show()

        Handler(Looper.getMainLooper()).postDelayed({
            val intent = Intent(this,ListMapButtonsActivity::class.java)

            startActivity(intent)
        }, 5000)


    }

    private fun showSuccessAlert2(){

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Do not worry!")
        builder.setMessage("You do not have any appointments yet")
        val dialog: AlertDialog = builder.create()
        dialog.show()

        Handler(Looper.getMainLooper()).postDelayed({
            val intent = Intent(this,ListMapButtonsActivity::class.java)

            startActivity(intent)
        }, 5000)


    }

    private fun showError() {
        Toast.makeText(this, "An error has occurred, please try again later", Toast.LENGTH_SHORT).show()
    }
}