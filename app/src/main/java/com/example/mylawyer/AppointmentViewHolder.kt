package com.example.mylawyer

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.mylawyer.databinding.ItemAppointmentBinding

class AppointmentViewHolder(view:View):RecyclerView.ViewHolder(view) {

    private val binding = ItemAppointmentBinding.bind(view)

    fun bind(appointment: Appointment){
        binding.textViewDescription.setText(appointment.description)
        binding.textViewDateTime.setText(appointment.dateHour)
    }
}