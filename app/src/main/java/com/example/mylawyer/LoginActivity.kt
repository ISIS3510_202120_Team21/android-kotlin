package com.example.mylawyer
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import android.content.SharedPreferences
import com.google.gson.Gson







class LoginActivity : AppCompatActivity() {


    private var lawyers = mutableListOf<Lawyer>()

    private lateinit var register: Button
    private lateinit var login: Button
    private lateinit var email: EditText
    private lateinit var password: EditText
    private var mAuth: FirebaseAuth? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_register)
        mAuth = FirebaseAuth.getInstance();

        register = findViewById(R.id.register)
        login = findViewById(R.id.login)
        email = findViewById(R.id.username)
        password = findViewById(R.id.password)
        setup()

    }

    private fun  setup(){
        title = "Authentication"

        register.setOnClickListener{
            if (email.text.isNotEmpty() && password.text.isNotEmpty()){
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(email.text.toString(),
                    password.text.toString()).addOnCompleteListener{
                    if(it.isSuccessful){
                        showSuccessAlert()
                        val intent = Intent(this,ListMapButtonsActivity::class.java)
                        saveUser(email.text.toString())
                        startActivity(intent)
                    }else{
                        showAlert()
                    }
                }
            }
        }

        login.setOnClickListener{
            if (email.text.isNotEmpty() && password.text.isNotEmpty()){
                FirebaseAuth.getInstance().signInWithEmailAndPassword(email.text.toString(),
                    password.text.toString()).addOnCompleteListener{
                    if(it.isSuccessful){
                        showSuccessAlert()
                        saveUser(email.text.toString())
                    }else{
                        showAlert()
                    }
                }
            }
        }
    }

    private fun getRetrofit(): Retrofit {
        val emailAddress = email.text.toString()
        return Retrofit.Builder()
            .baseUrl("https://mylawyer-flutter.herokuapp.com/api/clients/?emailAddress=$emailAddress")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun showAlert(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage("An error has ocurred while authenticating the user.")
        builder.setPositiveButton("Accept", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun saveUser(email: String){
        CoroutineScope(Dispatchers.IO).launch {
            val call: Response<ClientsResponse> = getRetrofit().create(ClientsAPIService::class.java).getAllClients("")
            val usersResponse : ClientsResponse? = call.body()
            Log.d("USER", usersResponse.toString())
            val usersList = usersResponse?.data?.dataList?: emptyList()
            if(usersList.isNotEmpty()){
                val user = usersList[0]
                val prefs = getSharedPreferences("PREFERENCES", Context.MODE_PRIVATE)
                val editor = prefs.edit()
                val gson = Gson()
                val json = gson.toJson(user)
                Log.d("USER", json)
                editor.putString("User",json)
                editor.apply()
            }
        }
    }



    private fun showSuccessAlert(){

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Welcome to MyLawyer")
        builder.setMessage("Remember to check your appointments")
        val dialog: AlertDialog = builder.create()
        dialog.show()

        Handler(Looper.getMainLooper()).postDelayed({
                val intent = Intent(this,ListMapButtonsActivity::class.java).apply {
                    putExtra("Data",email.text.toString())
                }
            startActivity(intent)
        }, 5000)

    }

}
