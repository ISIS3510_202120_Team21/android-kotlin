package com.example.mylawyer

import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface AppointmentsAPIService {

    @POST("appointments")
    fun addAppointment(@Body appointment: Appointment): Call<AppointmentsResponse>

    @GET
    suspend fun getAllAppointments(@Url url: String): Response<GetAppointmentsResponse>
}