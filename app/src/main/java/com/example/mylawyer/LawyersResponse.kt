package com.example.mylawyer

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class LawyersResponse(
    @SerializedName("status")
    var status: String? = null,
    @SerializedName("results")
    var results: Int = 0,
    @SerializedName("data")
    var data: Data? = null
)

data class Data(
    @SerializedName("data")
    var dataList: ArrayList<Lawyer>
)

data class Lawyer(
    @SerializedName("_id")
    var id: String? = null,
    @SerializedName("idAuth")
    var idAuth: String? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("phoneNumber")
    var phoneNumber: String? = null,
    @SerializedName("emailAddress")
    var emailAddress: String? = null,
    @SerializedName("casesWon")
    var casesWon: Int = 0,
    @SerializedName("field")
    var field: String? = null,
    @SerializedName("hourlyFee")
    var hourlyFee: String? = null,
    @SerializedName("rating")
    var rating: Int = 0,
    @SerializedName("numberOfRatings")
    var numberOfRatings: Int = 0,
    @SerializedName("latitude")
    var latitude: String? = null,
    @SerializedName("longitude")
    var longitude: String? = null,
    @SerializedName("views")
    var views: Int = 0,
    @SerializedName("yearOfExperience")
    var yearOfExperience: Int = 0
): Serializable

data class LawyerResponse(val code: Int?, val meta: String?, val data: Lawyer?)