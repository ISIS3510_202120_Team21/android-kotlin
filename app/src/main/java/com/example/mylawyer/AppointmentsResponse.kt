package com.example.mylawyer

import com.google.gson.annotations.SerializedName

data class AppointmentsResponse(
    @SerializedName("status")
    var status: String? = null,
    @SerializedName("data")
    var data: DataAppointments? = null
)

data class GetAppointmentsResponse(
    @SerializedName("status")
    var status: String? = null,
    @SerializedName("results")
    var results: Int = 0,
    @SerializedName("data")
    var data: DataGetAppointments? = null
)

data class DataAppointments(
    @SerializedName("data")
    var dataList: Appointment
)

data class DataGetAppointments(
    @SerializedName("data")
    var dataList: ArrayList<Appointment>
)

data class Appointment(
    @SerializedName("_id")
    val id: String? = null,
    @SerializedName("dateHour")
    var dateHour: String? = null,
    @SerializedName("description")
    var description: String? = null,
    @SerializedName("clientId")
    var clientId: String? = null,
    @SerializedName("lawyerId")
    var lawyerId: String? = null,
)