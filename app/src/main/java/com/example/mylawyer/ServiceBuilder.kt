package com.example.mylawyer

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ServiceBuilder {
    private val appointment = OkHttpClient.Builder().build()

    private val retrofit = Retrofit.Builder()
        .baseUrl("https://mylawyer-flutter.herokuapp.com/api/") // change this IP for testing by your actual machine IP
        .addConverterFactory(GsonConverterFactory.create())
        .client(appointment)
        .build()

    fun<T> buildService(service: Class<T>): T{
        return retrofit.create(service)
    }
}